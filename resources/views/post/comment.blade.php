@foreach( $comments as $comment)
<div class="comment">
    Autor: <b>{{ $comment->author }}</b> <small>| {{ $comment->created_at->format('d.m.Y H:i') }} #{{ $comment->id }}</small> 

    <p>{{ $comment->content }}</p>

    <button class="btn btn-secondary btn-sm" data-toggle="collapse" data-target="#collapseReplyForm-{{ $comment->id }}">Reply to this comment</button>

    @auth
        <a href="{{ route('comment.updateForm', ['comment'=>$comment]) }}" class="btn btn-secondary btn-sm">Edit</a>
        <form action="{{ route('comment.delete') }}" class="d-inline-block" method="post">
            @csrf
            <input type="hidden" name="comment_id" value="{{ $comment->id }}">
            <button type="submit" name="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete comment</button>
        </form>
    @endauth
    
    <div class="mt-3 ml-4 collapse" id="collapseReplyForm-{{ $comment->id }}">
    
        <form action="{{ route('comment.create') }}" method="post">
            @csrf
            <div class="form-group">
                <input type="text" class="form-control col-4" name="author" placeholder="Author name" value="{{ $preset_author }}">
            </div>
            <div class="form-group">
                <textarea type="text" class="form-control col-6" name="content" placeholder="Comment text"></textarea>
            </div>

            <input type="hidden" name="post_id" value="{{ $post_id }}">
            <input type="hidden" name="parent_comment_id" value="{{ $comment->id }}">

            <button type="submit" name="submit" class="btn btn-primary">Send</button>

        </form>
    </div>

    <hr>
    <div class="ml-4">
        @include('post.comment', ['comments' => $comment->replyComments])
    </div>
</div>
@endforeach