@extends('layouts.app')

@section('content')

<div class="container">
    <h1>{{ $post->title }}</h1>
    <small>{{ $post->created_at->format('d.m.Y H:i') }}</small>
    <div class="mt-2 mb-5">
        {{ $post->content }}
    </div>

    <h4>Comments</h4>


    @if ($post->comments->count() > 0)

        @include('post.comment', ['comments' => $post->comments, 'post_id' => $post->id])

    @else

        <p>No comments</p>
        
    @endif

    <h5>Leave comment</h5>

    @include('validationErrors')

    <form action="{{ route('comment.create') }}" method="post">
        @csrf
        <div class="form-group">
            <input type="text" class="form-control col-4" name="author" placeholder="Author name" value="{{ $preset_author }}">
        </div>
        <div class="form-group">
            <textarea type="text" class="form-control col-6" name="content" placeholder="Comment text"></textarea>
        </div>

        <input type="hidden" name="post_id" value="{{ $post->id }}">

        <button type="submit" name="submit" class="btn btn-primary">Send</button>

    </form>

</div>

@endsection