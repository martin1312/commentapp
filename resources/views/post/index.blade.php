@extends('layouts.app')

@section('content')

<div class="container">
    @foreach($posts as $post)
        <div class="my-3">
            <h3><a href="{{ route('post.single', $post->url) }}">{{ $post->title }}</a></h3>
        </div>
    @endforeach
</div>

@endsection