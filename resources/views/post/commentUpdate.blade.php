@extends('layouts.app')

@section('content')

<div class="container">

    <p><a href="{{ route('post.single', $comment->post) }}"> &lt; Back to post</a></p>

    <h2>Update comment</h2>   

    @include('validationErrors') 

    <form action="{{ route('comment.update') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Author</label>
            <input type="text" class="form-control col-4" name="author" value="{{ old('author', $comment->author) }}">
        </div>
        <div class="form-group">
            <label>Comment</label>
            <textarea type="text" class="form-control col-6" name="content" >{{ old('content', $comment->content) }}</textarea>
        </div>

        <input type="hidden" name="comment_id" value="{{ $comment->id }}">

        <button type="submit" name="submit" class="btn btn-primary">Update</button>

    </form>

    <form action="{{ route('comment.delete') }}" method="post" class="mt-3">
        @csrf
        <input type="hidden" name="comment_id" value="{{ $comment->id }}">

        <button type="submit" name="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete comment</button>

    </form>

</div>

@endsection