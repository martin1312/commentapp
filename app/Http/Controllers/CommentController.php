<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Comment;


class CommentController extends Controller
{
    public function create(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'content'           => 'required|min:5',
            'author'            => 'required|min:3',
            'post_id'           => 'required|numeric',
        ]);

        if($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $reply = new Comment();

        $reply->content = $req->get('content');
        $reply->author = $req->get('author');
        $reply->parent_id = $req->get('parent_comment_id') ?? null;

        if(Auth::check()) {
            $reply->user_id = Auth::user()->id;
        }

        $post = Post::findOrFail($req->get('post_id'));

        $post->comments()->save($reply);

        return back()->with('success', 'Comment created successfully');;
    }

    public function updateForm(Comment $comment) 
    {
        return view('post.commentUpdate', ['comment' => $comment]);
    }

    public function update(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'content'           => 'required|min:5',
            'author'            => 'required|min:3',
            'comment_id'        => 'required|numeric',
        ]);

        if($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $comment = Comment::findOrFail($req->get('comment_id'));

        $data = [
            'author' => $req->get('author'),
            'content' => $req->get('content'),
        ];

        $comment->update($data);

        return back()->with('success', 'Comment edited successfully');

    }

    public function delete(Request $req) 
    {
        $comment = Comment::findOrFail($req->get('comment_id'));
        $post = $comment->post;

        $comment->delete();

        return redirect()->route('post.single', ['post' => $post])
                                ->with('success', 'Comment deleted successfully');

    }

}
