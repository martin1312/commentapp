<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    public function index() 
    {
        $posts = Post::get();

        return view('post.index', ['posts'=>$posts]);
    }

    public function single(Post $post) 
    {
        $preset_author = '';

        if(Auth::check()) {
            $preset_author = Auth::user()->name;
        }

        return view('post.single', [
            'post'          => $post,
            'preset_author' => $preset_author,
        ]);
    }
}
