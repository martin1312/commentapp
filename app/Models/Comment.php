<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'author', 'content'
    ];

    public function replyComments()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function post() 
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
