<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $dates = ['created_at'];

    public function comments() 
    {
        return $this->hasMany(Comment::class, 'post_id')->whereNull('parent_id');
    }
}
