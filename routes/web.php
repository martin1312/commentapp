<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
]);

Route::get('/', 'PostController@index')->name('posts');
Route::get('/post/{post:url}', 'PostController@single')->name('post.single');
Route::post('/comment/create', 'CommentController@create')->name('comment.create');

Route::middleware(['auth'])->group(function() {
    Route::get('/comment/updateForm/{comment:id}', 'CommentController@updateForm')->name('comment.updateForm');
    Route::post('/comment/update', 'CommentController@update')->name('comment.update');
    Route::post('/comment/delete', 'CommentController@delete')->name('comment.delete');
});


